all: build
copy:
	@mkdir -p _bin
	@cp -uf _build/default/src/ppx/to_mligo.exe _bin/to_mligo
	@cp -uf _build/default/src/install/ligo_install.exe _bin/ligo_install

build:
	@dune build @install
	@$(MAKE) -s copy
dev:
	@dune build
	@$(MAKE) -s copy
clean:
	@dune clean
	@rm -rf _bin
deps:
	@opam update
	@opam install . --deps-only
