## Mligo Helper

Virtual library and ppx to allow to code mligo contract in ocaml using tuareg, merlin, ... and any ocaml environment.

To load the virtual library in your .ml file:
```ocaml
open Mligo
```

In your dune file, you just need to add the preprocessor command:
```lisp
(library
 ...
 (preprocess (pps mligo.ppx))
 ...)
```

The only thing not handled directly is the tez/mutez amount, you need to write:
`42t` for `42tez` and `42u` for `42mutez`.

To get the final .mligo file:
```bash
to_mligo <your_main_file.ml>
```
