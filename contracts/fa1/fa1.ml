open Mligo

let ctx = ref None
[@@ignore]

type transfer = {
  tr_src: address; [@key "from"]
  tr_dst: address; [@key "to"]
  tr_amount: nat; [@key "value"]
} [@@comb] [@@param Transfer]

type approve = {
  ap_spender: address;
  ap_value: nat
} [@@param Approve]

type account = {
  ac_balance: nat;
  ac_allowances: (address, nat) map;
} [@@comb] [@@param Store]

type allowance = {
  al_owner: address;
  al_spender: address;
}

[%%if_KYC
  type storage = {
    ledger: (address, account) big_map;
    token_metadata: (nat, (nat * (string, bytes) map)) big_map;
    supply: nat;
    kyc: address;
  } [@@comb] [@@store]
]
[%%else
  type storage = {
    ledger: (address, account) big_map;
    token_metadata: (nat, (nat * (string, bytes) map)) big_map;
    supply: nat;
  } [@@comb] [@@store]
]

type params =
  | Transfer of transfer
  | Approve of approve
[@@entry Main]

let get_account (s: storage) (addr: address) : account =
  [%mligo Format.printf "get_account %s" addr];
  match Big_map.find_opt addr s.ledger with
  | None -> { ac_balance = 0n; ac_allowances = (Map.empty : (address, nat) map) }
  | Some acc -> acc
[@@inline]

let get_allowance (owner: account) (spender: address) : nat =
  [%mligo Format.printf "get_allowance %s" spender];
  match Map.find_opt spender owner.ac_allowances with
  | None -> 0n
  | Some n -> n
[@@inline]

let transfer_aux (s: storage) (sd: address) (t: transfer) : storage =
  let ac = get_account s t.tr_src in
  match is_nat (ac.ac_balance - t.tr_amount) with
  | None -> (failwith "NotEnoughBalance" : storage)
  | Some ac_balance ->
    let ac =
      if t.tr_src <> sd then
        let allowance = get_allowance ac sd in
        match is_nat (allowance - t.tr_amount) with
        | None -> (failwith "NotEnoughAllowance" : account)
        | Some d ->
          let ac_allowances = Map.update sd (Some d) ac.ac_allowances in
          { ac_balance; ac_allowances }
      else { ac with ac_balance } in
    let s = { s with ledger = Big_map.update t.tr_src (Some ac) s.ledger } in
    let dest = get_account s t.tr_dst in
    let dest = { dest with ac_balance = dest.ac_balance + t.tr_amount } in
    { s with ledger = Big_map.update t.tr_dst (Some dest) s.ledger }

let approve_aux (s: storage) (sd: address) (a: approve) : storage =
  let ac = get_account s sd in
  let allowance = get_allowance ac a.ap_spender in
  let () = if allowance > 0n && a.ap_value > 0n then failwith "UnsafeAllowanceChange" in
  let ac = { ac with ac_allowances = Map.update a.ap_spender (Some a.ap_value) ac.ac_allowances } in
  { s with ledger = Big_map.update sd (Some ac) s.ledger }

[%%if_KYC
  let check_kyced (s: storage) (addr: address) : unit =
    match (Tezos.call_view !ctx "kyced" addr s.kyc : bool option) with
    | Some b -> if b then () else (failwith "NotKYCed" : unit)
    | _ -> (failwith "UnknownKYC" : unit)
]
[%%else
  let check_kyced (_s: storage) (_addr: address) : unit = ()
]

let transfer (t, s: transfer * storage) =
  let () = check_kyced s (Tezos.get_sender !ctx) in
  ([] : operation list), transfer_aux s (Tezos.get_sender !ctx) t

let approve (t, s: approve * storage) =
  let () = check_kyced s (Tezos.get_sender !ctx) in
  ([] : operation list), approve_aux s (Tezos.get_sender !ctx) t

let get_balance (addr, s : address * storage) : nat =
  let owner = get_account s addr in
  owner.ac_balance
[@@view]

let get_allowance (a, s : allowance * storage) : nat =
  let owner = get_account s a.al_owner in
  get_allowance owner a.al_spender
[@@view]

let get_supply ((), s : unit * storage) : nat =
  s.supply
[@@view]

let main (action, s: params * storage) : operation list * storage =
  match action with
  | Transfer t -> transfer (t, s)
  | Approve a -> approve (a, s)

let zero_tz1 = "tz1XvkuUNDk8j2tG3RJaRUo4Xppcjc6FvK39" [@@ignore]
let zero_kt1 = "KT1BEqzn5Wx8uJrZNvuS9DVHmLvG9td3fDLi" [@@ignore]

let tz1 =
  Tezos.mk_implicit_account ~balance:1000000 zero_tz1
[@@ignore]

[%%if_KYC
  let storage = {
    ledger = Big_map.empty;
    token_metadata = Big_map.literal [0, (0, Map.literal ["decimals", 0x32h])];
    supply = 1000000;
    kyc = None;
  } [@@ignore]
]
[%%else
  let storage = {
    ledger = Big_map.empty;
    token_metadata = Big_map.literal [0, (0, Map.literal ["decimals", 0x32h])];
    supply = 1000000;
  } [@@ignore]
]

let context = {
  Tezos.source = zero_tz1;
  sender = zero_tz1;
  self = zero_kt1;
  level = 0;
  time = 0;
  amount = 0;
  chain_id = "";
  voting_power = 0;
  contracts = [
    tz1;
    Tezos.mk_contract ~storage ~entrypoint:"transfer" ~address:zero_kt1 transfer;
    Tezos.mk_contract ~storage ~entrypoint:"approve" ~address:zero_kt1 approve;
  ];
  views = [
    Tezos.mk_view ~storage ~entrypoint:"get_balance" ~address:zero_kt1 get_balance;
    Tezos.mk_view ~storage ~entrypoint:"get_allowance" ~address:zero_kt1 get_allowance;
    Tezos.mk_view ~storage ~entrypoint:"get_supply" ~address:zero_kt1 get_supply;
  ];
  constants = []
} [@@ignore]

let init context = ctx := context
[@@ignore]
