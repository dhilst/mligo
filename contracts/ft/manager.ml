open Mligo
include Admin

let mint (a, s : nat * storage) : operation list * storage =
  let a = match Big_map.find_opt (Tezos.get_sender None) s.ledger with
    | Some a2 -> a + a2
    | None -> a in
  let ledger = Big_map.update (Tezos.get_sender None) (Some a) s.ledger in
  ([] : operation list), { s with ledger }

let burn (a, s : nat * storage)  : operation list * storage =
  match Big_map.find_opt (Tezos.get_sender None) s.ledger with
  | None -> (failwith fa2_not_owner : operation list * storage)
  | Some am -> match is_nat (am - a) with
    | None -> (failwith fa2_insufficient_balance : operation list * storage)
    | Some am2 ->
      let ledger = Big_map.update (Tezos.get_sender None) (Some am2) s.ledger in
      ([] : operation list), { s with ledger }

let manager (param, s : manager * storage) : operation list * storage =
  match param with
  | Mint a ->
    let () = fail_if_not_admin s in
    mint (a, s)
  | Burn a -> burn (a, s)
