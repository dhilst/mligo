open Mligo
include Types

let get_balance (p, s : balance_of_param * storage) : balance_of_response list =
  List.map (fun (ba_request : balance_of_request) ->
      if ba_request.ba_token_id <> 0n then (failwith fa2_token_undefined : balance_of_response)
      else match Big_map.find_opt ba_request.ba_owner s.ledger with
        | None -> {ba_request; ba_balance = 0n}
        | Some ba_balance -> {ba_request; ba_balance}) p.ba_requests
[@@view]

let balance_of (p, s : balance_of_param * storage) : operation list * storage =
  let l = List.map (fun (ba_request : balance_of_request) ->
      if ba_request.ba_token_id <> 0n then (failwith fa2_token_undefined : balance_of_response)
      else match Big_map.find_opt ba_request.ba_owner s.ledger with
        | None -> { ba_request; ba_balance = 0n }
        | Some ba_balance -> { ba_request; ba_balance }) p.ba_requests in
  [ Tezos.transaction None l 0u p.ba_callback ], s

let validate (owner : address) (operator : address) (s : storage) : unit =
  if owner = operator then ()
  else if Big_map.mem (owner, operator) s.operators then ()
  else (failwith fa2_not_operator : unit)

let transfer (txs, s : transfer list * storage) : operation list * storage =
  let ledger = List.fold (fun (l, tx : ledger * transfer) ->
      List.fold (fun (ll, dst : ledger * transfer_destination) ->
          if dst.tr_token_id <> 0n then (failwith fa2_token_undefined : ledger)
          else match Big_map.find_opt tx.tr_src ll with
          | None -> (failwith fa2_insufficient_balance : ledger)
          | Some am ->
            if dst.tr_amount = 0n then ll
            else match is_nat (am - dst.tr_amount) with
              | None -> (failwith fa2_insufficient_balance : ledger)
              | Some diff ->
                let () = validate tx.tr_src (Tezos.get_sender None) s in
                let ll =
                  if diff = 0n then Big_map.remove tx.tr_src ll
                  else Big_map.update tx.tr_src (Some diff) ll in
                match Big_map.find_opt dst.tr_dst ll with
                | None -> Big_map.add dst.tr_dst dst.tr_amount ll
                | Some am -> Big_map.update dst.tr_dst (Some (am + dst.tr_amount)) ll)
        tx.tr_txs l)
      txs s.ledger in
  ([] : operation list), { s with ledger }

let update_operator (update, storage : operator_update * storage) : storage =
  match update with
  | Add_operator update ->
    if update.op_token_id <> 0n then (failwith fa2_token_undefined : storage)
    else
      let operators = Big_map.update (update.op_owner, update.op_operator) (Some ()) storage.operators in
      { storage with operators }
  | Remove_operator update ->
    if update.op_token_id <> 0n then (failwith fa2_token_undefined : storage)
    else
      let operators = Big_map.remove (update.op_owner, update.op_operator) storage.operators in
      { storage with operators }

let validate_update_operators_by_owner (update : operator_update) (updater : address)
  : unit =
  let op = match update with
    | Add_operator op -> op
    | Remove_operator op -> op in
  if op.op_owner = updater then () else failwith fa2_not_owner

let update_operators (ops, storage : operator_update list * storage) : operation list * storage =
  let storage = List.fold (fun (storage, update : storage * operator_update) ->
      let () = validate_update_operators_by_owner update (Tezos.get_sender None) in
      update_operator (update, storage)) ops storage in
  ([] : operation list), storage

let fa2 (param, storage : fa2 * storage) : (operation  list) * storage =
  match param with
  | Transfer txs -> transfer (txs, storage)
  | Update_operators ops -> update_operators (ops, storage)
  | Balance_of p -> balance_of (p, storage)
