let ligo_prefix = match Sys.getenv_opt "LIGO_PREFIX" with
  | None -> "/usr/local/bin"
  | Some s -> s

let static_links = [
  "dev", "https://ligolang.org/bin/linux/ligo";
  "0.45.0", "https://gitlab.com/ligolang/ligo/-/jobs/2632723176/artifacts/raw/ligo";
  "0.44.0", "https://gitlab.com/ligolang/ligo/-/jobs/2563037588/artifacts/raw/ligo";
]

let version = ref "0.45.0"

let installed () =
  let stdout = Filename.temp_file "ligo_install_version" "" in
  Sys.command @@ Filename.quote_command ~stdout "ligo" ["--version"] = 0

let static_install () =
  let link = Option.value ~default:"https://ligolang.org/bin/linux/ligo" @@
    List.assoc_opt !version static_links in
  let r = Sys.command @@ Format.sprintf
      "curl -s -o ligo %s && \
       chmod +x ligo && mv ligo %s" link ligo_prefix in
  if r <> 0 then Format.printf "Ligo install failed@."

let docker_install () =
  let oc = open_out_gen [ Open_wronly; Open_text; Open_creat; Open_trunc ] 0o555 @@
    Filename.concat ligo_prefix "ligo" in
  output_string oc
    (Format.sprintf "#!/bin/sh\ndocker run --rm -v $PWD:$PWD -w $PWD ligolang/ligo:%s $@" !version);
  close_out oc

let main () =
  Array.iter (fun s ->
      let n = String.length s in
      if n >= 10 && String.sub s 0 10 = "--version=" then version := String.sub s 10 (n - 10)) Sys.argv;
  let docker = Array.exists (function "--docker" -> true | _ -> false) Sys.argv in
  let ic = Unix.open_process_in "uname" in
  let uname = String.trim @@ input_line ic in
  close_in ic;
  if uname = "Linux" && not docker then static_install ()
  else docker_install ()

let () =
  let reset = Array.exists (function "--reset" -> true | _ -> false) Sys.argv in
  if not reset && installed () then ()
  else if reset then
    let _ = Sys.command @@ Format.sprintf "rm -f %s/ligo" ligo_prefix in
    main ()
  else main ()
