open Ppxlib
open Ast_builder.Default

(* let ligo_exprs : (string, expression) Hashtbl.t = Hashtbl.create 512
 *
 * let add_expr id expr =
 *   match Hashtbl.find_opt ligo_exprs id with
 *   | None -> Hashtbl.add ligo_exprs id expr
 *   | Some _ -> Location.raise_errorf "Parameter ident %S is already used" id *)

(* let to_mligo_integer_expr expr =
 *   match expr.pexp_desc with
 *   | Pexp_constant (Pconst_integer (s, Some 't')) ->
 *     evar ~loc:expr.pexp_loc (s ^ "tez")
 *   | Pexp_constant (Pconst_integer (s, Some 'u')) ->
 *     evar ~loc:expr.pexp_loc (s ^ "mutez")
 *   | Pexp_constant (Pconst_integer (s, Some 'h')) ->
 *     if String.length s > 2 && String.sub s 0 2 = "0x" then
 *       evar ~loc:expr.pexp_loc (String.sub s 0 (String.length s - 1))
 *     else expr
 *   | Pexp_constant (Pconst_integer (s, None)) ->
 *     if String.length s > 2 && String.sub s 0 2 = "0x" then
 *       evar ~loc:expr.pexp_loc (String.sub s 0 (String.length s - 1))
 *     else expr
 *   | _ -> expr
 *
 * let to_mligo_integer_pat p =
 *   let loc = p.ppat_loc in
 *   match p.ppat_desc with
 *   | Ppat_constant (Pconst_integer (s, Some 't')) ->
 *     pvar ~loc (s ^ "tez")
 *   | Ppat_constant (Pconst_integer (s, Some 'u')) ->
 *     pvar ~loc (s ^ "mutez")
 *   | Ppat_constant (Pconst_integer (s, Some 'h')) ->
 *     if String.length s > 2 && String.sub s 0 2 = "0x" then
 *       pvar ~loc (String.sub s 0 (String.length s - 1))
 *     else p
 *   | Ppat_constant (Pconst_integer (s, None)) ->
 *     if String.length s > 2 && String.sub s 0 2 = "0x" then
 *       pvar ~loc (String.sub s 0 (String.length s - 1))
 *     else p
 *   | _ -> p *)

(* let to_mligo =
 *   object(_self)
 *     inherit Ast_traverse.map as super
 *     method! expression expr =
 *       match expr.pexp_desc with
 *       | Pexp_constant (Pconst_integer _) -> to_mligo_integer_expr expr
 *       | Pexp_record (l, e) ->
 *         let l = List.map (function
 *             | (id1, ({pexp_desc = Pexp_ident id2; _} as e)) when id1.txt = id2.txt ->
 *               let id2 = {loc=id2.loc; txt = Longident.parse @@ (Longident.name id2.txt ^ " ")} in
 *               (id1, {e with pexp_desc = Pexp_ident id2})
 *             | (id, ({pexp_desc = Pexp_constant (Pconst_integer _); _} as e)) ->
 *               (id, to_mligo_integer_expr e)
 *             | (id, e) ->
 *               (id, super#expression e)) l in
 *         {expr with pexp_desc = Pexp_record (l, Option.map super#expression e)}
 *       | Pexp_field (e, lid) ->
 *         let e = super#expression e in
 *         let txt = Longident.parse @@
 *           (Pprintast.string_of_expression e) ^ "." ^ Longident.name lid.txt in
 *         { e with pexp_desc = Pexp_ident {lid with txt} }
 *       | Pexp_ident {txt = Lident s; _} ->
 *         begin match Hashtbl.find_opt ligo_exprs s with
 *           | None -> super#expression expr
 *           | Some expr -> super#expression expr
 *         end
 *       | _ ->
 *         super#expression expr
 *
 *     method! pattern p =
 *       match p.ppat_desc with
 *       | Ppat_constant (Pconst_integer _) -> to_mligo_integer_pat p
 *       | _ -> super#pattern p
 *
 *   end *)

let defines : (string, unit) Hashtbl.t = Hashtbl.create 512

let rec choose_if_extension f = function
  | {pexp_desc=Pexp_extension (
      {txt; _}, PStr [{pstr_desc=Pstr_eval (e, _); _} ]); _} :: q ->
    if String.length txt > 3 && String.sub txt 0 2 = "if" then
      let env_var = String.sub txt 3 (String.length txt - 3) in
      match Sys.getenv_opt env_var, Hashtbl.find_opt defines env_var with
      | Some "true", _ | Some "1", _ | _, Some () -> f e
      | _ -> choose_if_extension f q
    else if String.length txt > 5 && String.sub txt 0 4 = "elif" then
      let env_var = String.sub txt 5 (String.length txt - 5) in
      match Sys.getenv_opt env_var, Hashtbl.find_opt defines env_var with
      | Some "true", _ | Some "1", _ | _, Some () -> f e
      | _ -> choose_if_extension f q
    else if txt = "else" then f e
    else Location.raise_errorf "cannot understand [%%if] extension"
  | _ -> Location.raise_errorf "cannot understand [%%if] extension"

let to_ml =
  object(_self)
    inherit Ast_traverse.map as super

    method! expression expr =
      let aux expr =
        let loc = expr.pexp_loc in
        match expr.pexp_desc with
        | Pexp_constant (Pconst_integer (s, Some 't')) ->
          let i = Int64.of_string s in
          let s = Int64.(to_string (mul i 1000000L)) in
          {expr with pexp_desc = Pexp_constant (Pconst_integer (s, None)) }
        | Pexp_constant (Pconst_integer (s, Some 'u')) ->
          {expr with pexp_desc = Pexp_constant (Pconst_integer (s, None)) }
        | Pexp_constant (Pconst_integer (s, Some 'n')) ->
          {expr with pexp_desc = Pexp_constant (Pconst_integer (s, None)) }
        | Pexp_constant (Pconst_integer (s, Some 'h')) ->
          {expr with pexp_desc = Pexp_constant (Pconst_string (s, loc, None)) }
        | _ -> super#expression expr in
      let loc = expr.pexp_loc in
      match expr.pexp_desc with
      | Pexp_apply ({pexp_desc=Pexp_extension (
          {txt; _}, _); _} as e, l) when String.length txt > 3 && String.sub txt 0 2 = "if" ->
        choose_if_extension aux (e :: List.map snd l)
      | Pexp_extension ({txt="Michelson"; _}, PStr [{pstr_desc=Pstr_eval ({
          pexp_desc=Pexp_constraint ({pexp_desc=Pexp_constant (Pconst_string (_, _, _)); _}, t); _}, _); _}]) ->
        let rec aux t = match t.ptyp_desc with
          | Ptyp_arrow (_, t, _) -> pexp_fun ~loc Nolabel None (ppat_any ~loc) (aux t)
          | _ -> eapply ~loc (evar ~loc "failwith") [ estring ~loc "[%michelson] extension" ] in
        pexp_constraint ~loc (aux t) t
      | Pexp_extension ({txt="mligo"; _}, PStr [{pstr_desc=Pstr_eval (e, _); _}]) -> e
      | _ -> aux expr

    method! pattern p =
      let loc = p.ppat_loc in
      match p.ppat_desc with

      | Ppat_constant (Pconst_integer (s, Some 't')) ->
        let i = Int64.of_string s in
        let s = Int64.(to_string (mul i 1000000L)) in
        {p with ppat_desc = Ppat_constant (Pconst_integer (s, None)) }
      | Ppat_constant (Pconst_integer (s, Some 'u')) ->
        {p with ppat_desc = Ppat_constant (Pconst_integer (s, None)) }
      | Ppat_constant (Pconst_integer (s, Some 'n')) ->
        {p with ppat_desc = Ppat_constant (Pconst_integer (s, None)) }
      | Ppat_constant (Pconst_integer (s, Some 'h')) ->
        {p with ppat_desc = Ppat_constant (Pconst_string (s, loc, None)) }
      | _ -> super#pattern p

    (* method! value_binding vb =
     *   match List.exists (fun a -> a.attr_name.txt = "mligo") vb.pvb_attributes, vb.pvb_pat.ppat_desc with
     *   | true, Ppat_var {txt=id; _} ->
     *     add_expr id vb.pvb_expr;
     *     value_binding ~loc:vb.pvb_loc ~pat:(ppat_any ~loc:vb.pvb_loc) ~expr:(super#expression vb.pvb_expr)
     *   | _ -> super#value_binding vb *)

    method! structure s =
      let rec aux = function
        | (Some env_var, l) :: t ->
          begin match Sys.getenv_opt env_var, Hashtbl.find_opt defines env_var with
            | Some "true", _ | Some "1", _ | _, Some () -> l
            | _ -> aux t
          end
        | [ None, l ] -> l
        | _ -> Location.raise_errorf "missing [%%%%if] or [%%%%else]" in
      let rec aux2 s =
        let s, _ = List.fold_left (fun (acc, acc_ext) x ->
            match x.pstr_desc with
            | Pstr_extension (({txt; _}, PStr s), _) ->
              if String.length txt > 3 && String.sub txt 0 2 = "if" && acc_ext = [] then
                let env_var = String.sub txt 3 (String.length txt - 3) in
                (acc, [Some env_var, aux2 s])
              else if String.length txt > 5 && String.sub txt 0 4 = "elif" && acc_ext <> [] then
                let env_var = String.sub txt 5 (String.length txt - 5) in
                (acc, (Some env_var, aux2 s) :: acc_ext)
              else if txt = "else" then
                let acc_ext = List.rev @@ (None, aux2 s) :: acc_ext in
                let l = aux acc_ext in
                (acc @ l, [])
              else Location.raise_errorf "extenstion not handled"
            | Pstr_attribute a when a.attr_name.txt = "define" ->
              begin match a.attr_payload with
                | PStr [{pstr_desc=Pstr_eval ({pexp_desc=Pexp_construct ({txt=Lident x;_}, None); _}, _); _}]
                | PStr [{pstr_desc=Pstr_eval ({pexp_desc=Pexp_ident {txt=Lident x;_}; _}, _); _}] ->
                  Hashtbl.add defines x ()
                | _ -> ()
              end;
              acc, []
            | _ ->
              if acc_ext = [] then acc @ [super#structure_item x], []
              else Location.raise_errorf "something wrong in [%%%%if] extenstion"
          ) ([], []) s in
        s in
      aux2 s
  end

let () =
  Ppxlib.Driver.register_transformation "mligo" ~impl:to_ml#structure
