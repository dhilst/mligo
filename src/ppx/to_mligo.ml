open Ppxlib
open Ast_builder.Default

let dir = ref ""
let keep_prefix = ref false
let ocp_indent = ref false

let field_alias : (string, string) Hashtbl.t = Hashtbl.create 512

let rec transform_expr f expr =
  let loc = expr.pexp_loc in
  match expr.pexp_desc with
  | Pexp_constant (Pconst_integer (s, Some 't')) ->
    evar ~loc (s ^ "tez")
  | Pexp_constant (Pconst_integer (s, Some 'u')) ->
    evar ~loc (s ^ "mutez")
  | Pexp_constant (Pconst_integer (s, Some 'h')) ->
    evar ~loc s
  | Pexp_record (l, e) ->
    let l = List.map (function
        | (id1, ({pexp_desc = Pexp_ident id2; _} as e)) when id1.txt = id2.txt ->
          let id2 = {loc=id2.loc; txt = Longident.parse @@ (Longident.name id2.txt ^ " ")} in
          let id1_alias = match Hashtbl.find_opt field_alias (Longident.name id1.txt) with
            | None -> id1
            | Some alias -> { id1 with txt = Lident alias } in
          (id1_alias, {e with pexp_desc = Pexp_ident id2})
        | (id, e) ->
          let id_alias = match Hashtbl.find_opt field_alias (Longident.name id.txt) with
            | None -> id
            | Some alias -> { id with txt = Lident alias } in
          (id_alias, transform_expr f e)) l in
    {expr with pexp_desc = Pexp_record (l, Option.map (transform_expr f) e)}
  | Pexp_field (e, lid) ->
    let e = transform_expr f e in
    let lid_name = Longident.name lid.txt in
    let lid_alias = match Hashtbl.find_opt field_alias lid_name with
      | None -> lid_name
      | Some alias -> alias in
    let txt = Longident.parse @@
      (Pprintast.string_of_expression e) ^ "." ^ lid_alias in
    { e with pexp_desc = Pexp_ident {lid with txt} }
  | Pexp_ident {txt; loc} ->
    let id = Longident.name txt in
    let id =
      if id = "_and" then "and"
      else if id = "_or" then "or"
      else if id = "failwith_int" then "failwith"
      else id in
    evar ~loc id
  | Pexp_apply ({pexp_desc=Pexp_ident {txt; loc}; _}, _h :: q) ->
    let txt = Longident.name txt in
    let aux0 s =
      let args = if q = [] then [ Nolabel, eunit ~loc ] else q in
      f (pexp_apply ~loc (evar ~loc ("Tezos." ^ s)) args) in
    let rec aux = function
      | [] -> f expr
      | h :: t ->
        if txt = h || txt = "Tezos." ^ h then aux0 h
        else aux t in
    aux [ "get_balance"; "get_now"; "get_amount"; "get_sender"; "get_self_address";
          "get_source"; "get_level"; "get_chain_id"; "get_total_voting_power";
          "self"; "voting_power"; "transaction"; "set_delegate"; "get_contract_opt";
          "get_contract_with_error"; "get_entrypoint_opt"; "get_entrypoint";
          "implicit_account"; "open_chest"; "call_view"; "create_contract"; "constant" ]
  | Pexp_coerce (e, _, _) -> f e
  | Pexp_sequence ({pexp_desc=Pexp_extension ({txt="mligo"; _}, _); _}, e) -> f e
  | _ -> f expr

let rec transform_pat f p =
  let loc = p.ppat_loc in
  match p.ppat_desc with
  | Ppat_constant (Pconst_integer (s, Some 't')) ->
    pvar ~loc (s ^ "tez")
  | Ppat_constant (Pconst_integer (s, Some 'u')) ->
    pvar ~loc (s ^ "mutez")
  | Ppat_constant (Pconst_integer (s, Some 'h')) ->
    pvar ~loc s
  | Ppat_record (l, flag) ->
    let l = List.map (function
        | (id, p) ->
          let id_alias = match Hashtbl.find_opt field_alias (Longident.name id.txt) with
            | None -> id
            | Some alias -> { id with txt = Lident alias } in
          (id_alias, transform_pat f p)) l in
    {p with ppat_desc = Ppat_record (l, flag)}
  | Ppat_constraint (pat, ({ptyp_desc=Ptyp_constr (lid, [pa; _st]); _} as c)) ->
    let name = Longident.name lid.txt in
    if name = "contract" || name = "Tezos.contract" then
      f { p with ppat_desc = Ppat_constraint
                   (pat, {c with ptyp_desc = Ptyp_constr (lid, [pa]) }) }
    else f p
  | _ -> f p

let remove_prefix s n = String.sub s n (String.length s - n)

let same_prefix l =
  let common_prefix s1 s2 =
    let n1 = String.length s1 in
    let n2 = String.length s2 in
    let rec aux i =
      if i < n1 && i < n2 && s1.[i] = s2.[i] then aux (i+1)
      else i, String.sub s1 0 i in
    aux 0 in
  let rec aux n pr = function
    | [] -> n, pr
    | h :: t ->
      let n, pr = common_prefix h pr in
      aux n pr t in
  match l with
  | [] | [ _ ] -> 0
  | h :: t -> fst (aux (String.length h) h t)

let transform_type fpld fpcd fcore p =
  let ptype_attributes = List.filter (fun a -> a.attr_name.txt = "comb") p.ptype_attributes in
  match p.ptype_kind, p.ptype_manifest with
  | Ptype_record l, _ ->
    let l = List.map (fun pld ->
        let name = pld.pld_name.txt in
        let alias =
          List.fold_left (fun acc a -> match a.attr_name.txt, a.attr_payload with
              | "key", PStr [ {pstr_desc = Pstr_eval (
                  {pexp_desc = Pexp_constant (Pconst_string (s, _, _)); _}, _); _} ] ->
                Hashtbl.add field_alias name s;
                Some s
              | _ -> acc) None pld.pld_attributes in
        match alias with
        | None ->
          if !keep_prefix then
            fpld { pld with pld_attributes = [] } []
          else
            let n = same_prefix @@ List.map (fun pld -> pld.pld_name.txt) l in
            if n > 0 then
              let txt = remove_prefix name n in
              Hashtbl.add field_alias name txt;
              fpld { pld with pld_attributes = []; pld_name = {pld.pld_name with txt} } []
            else
              fpld { pld with pld_attributes = [] } []
        | Some txt -> fpld { pld with pld_attributes = []; pld_name = {pld.pld_name with txt} } []) l in
    let l = List.map fst l in
    { p with ptype_attributes; ptype_kind = Ptype_record l;
             ptype_private=Public}
  | Ptype_variant l, _ ->
    { p with ptype_attributes;
             ptype_kind = Ptype_variant (List.map (fun pcd -> fst (fpcd pcd [])) l);
             ptype_private=Public }
  | Ptype_abstract, Some m ->
    { p with ptype_attributes;
             ptype_kind = Ptype_abstract;
             ptype_manifest = Some (fst (fcore m []));
             ptype_private=Public }
  | _ ->
    {p with ptype_attributes; ptype_private=Public}

let value_to_string ?(first=false) ?(r_flag=Nonrecursive) pat expr attrs =
  let inline = List.fold_left (fun acc a ->
      acc || a.attr_name.txt = "inline") false attrs in
  let view = List.fold_left (fun acc a ->
      acc || a.attr_name.txt = "view") false attrs in
  let le =
    if first && r_flag = Recursive then "let rec"
    else if first then "let"
    else if r_flag = Recursive then "and"
    else "let" in
  let pat, cons0 = match pat.ppat_desc with
    | Ppat_constraint (p, c) -> p, Some c
    | _ -> pat, None in
  Pprintast.pattern Format.str_formatter pat;
  let name = Format.flush_str_formatter () in
  let rec aux acc expr = match expr.pexp_desc with
    | Pexp_fun (_, _, p, e) -> aux (p :: acc) e
    | Pexp_constraint (e, c) -> acc, e, Some c
    | _ -> acc, expr, None in
  let args, expr, cons = aux [] expr in
  let expr = Pprintast.string_of_expression expr in
  let cons = match cons, cons0 with
    | None, None -> ""
    | Some c, _  | _, Some c ->
      Pprintast.core_type Format.str_formatter c;
      " : " ^ Format.flush_str_formatter () in
  let args = String.concat " " @@ List.map (fun p ->
      Pprintast.pattern Format.str_formatter p;
      Format.flush_str_formatter ()) (List.rev args) in
  Format.sprintf "%s%s%s %s %s%s =\n%s"
    (if inline then "\n[@inline]\n" else "")
    (if view then "\n[@view]\n" else "")
    le name args cons expr

let rec transform () =
  object(_self)
    inherit [string list] Ast_traverse.fold_map as super

    method! expression e _ =
      transform_expr (fun e -> fst @@ super#expression e []) e, []

    method! pattern p _ =
      transform_pat (fun p -> fst @@ super#pattern p []) p, []

    method! core_type c _ =
      let loc = c.ptyp_loc in
      match c.ptyp_desc with
      | Ptyp_constr (lid, [pa; _st]) ->
        let name = Longident.name lid.txt in
        if name = "contract" || name = "Tezos.contract" then
          super#core_type {c with ptyp_desc = Ptyp_constr (lid, [pa]) } []
        else super#core_type c []
      | Ptyp_var s ->
        super#core_type
          { c with ptyp_desc = Ptyp_constr ({txt=Lident ("_" ^ s); loc}, []) } []
      | _ ->
        super#core_type c []

    method! constructor_declaration p acc =
      super#constructor_declaration {p with pcd_attributes = []} acc

    method! structure l _ =
      let rec aux l =
        List.fold_left (fun acc st ->
            match st.pstr_desc with
            | Pstr_open _ -> acc
            | Pstr_include {pincl_mod = {pmod_desc = Pmod_ident {txt = Lident id; _}; _}; _} ->
              let name = String.uncapitalize_ascii id in
              process name;
              acc @ [ Pprintast.string_of_structure [st] ]
            | Pstr_module {pmb_name = {txt = Some _; _};
                           pmb_expr = {pmod_desc = Pmod_ident {txt=Lident id; _}; _}; _} ->
              let name = String.uncapitalize_ascii id in
              process name;
              acc @ [ Pprintast.string_of_structure [st] ]
            | Pstr_module ({pmb_expr = {pmod_desc = Pmod_constraint (m, _); _}; _} as pmb) ->
              let pmb_expr, _ = super#module_expr m [] in
              acc @ [ Pprintast.string_of_structure [
                  { st with pstr_desc = Pstr_module {pmb with pmb_expr} } ] ]
            | Pstr_value (r_flag, pvl) ->
              let l, _ = List.fold_left (fun (acc, first) pv ->
                  if not (List.exists (fun a -> a.attr_name.txt = "ignore") pv.pvb_attributes) then
                    let pv, _ = super#value_binding pv [] in
                    acc @ [value_to_string ~first ~r_flag pv.pvb_pat pv.pvb_expr pv.pvb_attributes], false
                  else
                    acc, first) ([], true) pvl in
              if l = [] then acc
              else acc @ l
            | Pstr_type (r_flag, l) ->
              let l = List.filter_map (fun t ->
                  if not (List.exists (fun a -> a.attr_name.txt = "ignore") t.ptype_attributes) then
                    Some (transform_type super#label_declaration super#constructor_declaration super#core_type t)
                  else
                    None) l in
              if l = [] then acc
              else (
                let s2 = Pprintast.string_of_structure [{st with pstr_desc = Pstr_type (r_flag, l)}] in
                acc @ [ s2 ])
            | Pstr_modtype _ -> acc
            | Pstr_extension (({txt; _}, PStr s) , _attrs) ->
              let acc2 = aux s in
              if acc2 = [] then acc
              else acc @ [ Format.sprintf "[%%%%%s\n%s]" txt (String.concat "\n\n" acc2) ]
            | _ ->
              acc @ [ Pprintast.string_of_structure @@ [fst @@ super#structure_item st [] ] ]) [] l in
      l, aux l
  end

and process name =
  let path = Filename.concat !dir name in
  let ic = open_in (path ^ ".ml") in
  let s = really_input_string ic (in_channel_length ic) in
  close_in ic;
  let lexbuf = Lexing.from_string s in
  let str = Parse.implementation lexbuf in
  let _, l = (transform ())#structure str [] in
  let s = String.concat "\n\n" l in
  let mligo_name = name ^ ".tmp" in
  let oc = open_out mligo_name in
  output_string oc s;
  close_out oc;
  let stdout = path ^ ".tmp2" in
  if !ocp_indent then
    ignore @@ Sys.command @@ Filename.quote_command ~stdout "ocp-indent" [ path ^ ".tmp" ]
  else
    ignore @@ Sys.command (Format.sprintf "cp %s.tmp %s.tmp2" path path);
  let ic = open_in @@ Format.sprintf "%s.tmp2" path in
  let s = really_input_string ic (in_channel_length ic) in
  close_in ic;
  let r = Str.regexp "\\\n.+\\[@inline\\]" in
  let s = Str.global_replace r ("\n[@inline]") s in
  let r = Str.regexp "\\\n.+\\[@view\\]" in
  let s = Str.global_replace r ("\n[@view]") s in
  let s = Str.global_replace (Str.regexp ";;") "" s in
  let any_reg = "\\([A-Za-z0-9]\\|!\\|\"\\|#\\|\\$\\|%\\|&\\|'\\|(\\|)\\|\\*\\|\\+\\|,\\|\\.\\|/\\|:\\|;\\|<\\|=\\|>\\|\\?\\|@\\|\\\\\\|\\^\\|_\\|`\\|{\\||\\|}\\|~\\|-\\| \\|\\\n\\|\\(\\[.*\\]\\)\\)*" in
  let any_type_aux = "\\([A-Za-z0-9]\\|\"\\|'\\|(\\|)\\|\\*\\|,\\|:\\|;\\|_\\| \\|\\\n\\|\\.\\|->\\)+" in
  let any_type = "\\({" ^ any_type_aux ^ "}\\|\\( *| " ^ any_type_aux ^ "\\)+\\)" in
  let any_id = "\\([A-Za-z0-9]\\|_\\)+" in
  let r = Str.regexp @@ "include " ^ any_id ^ "\\(\\\n\\| \\)+" in
  let s = Str.global_substitute r (fun s ->
      let s = String.trim @@ Str.matched_string s in
      let name = String.uncapitalize_ascii @@ String.sub s 8 (String.length s - 8) in
      "#include \"" ^ name ^ ".mligo\"\n") s in
  let r = Str.regexp @@ "module " ^ any_id ^ " *= *" ^ any_id ^ "\\(\\\n\\| \\)+" in
  let s = Str.global_substitute r (fun s ->
      let s = String.trim @@ Str.matched_string s in
      let s = String.sub s 7 (String.length s - 7) in
      let i = String.index s '=' in
      let s1, s2 = String.trim (String.sub s 0 (i-1)), String.trim (String.sub s (i+1) (String.length s - i - 1)) in
      let s1 = String.capitalize_ascii s1 in
      let s2 = (String.uncapitalize_ascii s2) ^ ".mligo" in
      Format.sprintf "#import %S %S\n" s2 s1) s in
  let r = Str.regexp @@
    "type " ^ any_id ^ " =\\( \\|\\\n\\)*" ^
    any_type ^ "\\( \\|\\\n\\)*\\[@@comb *\\]\\(\\\n\\| \\)*" in
  let s = Str.global_substitute r (fun s ->
      let s = String.trim @@ Str.matched_string s in
      let i = String.index s '=' in
      let t = String.trim (String.sub s (i+1) (String.length s - i - 1)) ^ "\n\n" in
      let t =
        if String.get t 0 = '|' then "\n  |" ^ String.sub t 1 (String.length t - 1)
        else if String.get t 0 = '{' then " {" ^ String.sub t 1 (String.length t - 1)
        else t in
      String.trim (String.sub s 0 (i-1)) ^ " = [@layout:comb] " ^ t) s in
  let s = Str.global_replace (Str.regexp "\\[@@comb *\\]") "" s in
  let r = Str.regexp @@ " *\\[%%else\\( \\|\\\n\\)" ^ any_reg ^ "\\]\\\n*" in
  let s = Str.global_substitute r (fun s ->
      let s = String.trim @@ Str.matched_string s in
      "#else\n" ^ String.trim (String.sub s 7 (String.length s - 8)) ^ "\n#endif\n\n") s in
  let r = Str.regexp @@ "(\\[%else\\( \\|\\\n\\)" ^ any_reg ^ "\\])\\\n*" in
  let s = Str.global_substitute r (fun s ->
      let s = String.trim @@ Str.matched_string s in
      "\n#else\n" ^ String.trim (String.sub s 7 (String.length s - 9)) ^ "\n#endif\n") s in
  let return_after_env s =
    let i = match String.index_opt s ' ', String.index_opt s '\n' with
      | Some i, Some j -> min i j
      | Some i, _ | _, Some i -> i
      | _ -> -1 in
    if i = -1 then ""
    else String.sub s 0 i ^ "\n" ^ String.trim (String.sub s (i+1) (String.length s - i - 1)) in
  let r = Str.regexp @@ " *\\[%%elif_\\([A-Za-z0-9]\\|_\\)+\\( \\|\\\n\\)" ^ any_reg ^ "\\]\\\n*" in
  let s = Str.global_substitute r (fun s ->
      let s = String.trim @@ Str.matched_string s in
      let s = return_after_env @@ String.trim (String.sub s 8 (String.length s - 9)) in
      "#elif " ^ s ^ "\n") s in
  let r = Str.regexp @@ "(\\[%elif_\\([A-Za-z0-9]\\|_\\)+\\( \\|\\\n\\)" ^ any_reg ^ "\\])\\\n*" in
  let s = Str.global_substitute r (fun s ->
      let s = String.trim @@ Str.matched_string s in
      let s = return_after_env @@ String.trim @@ String.sub s 8 (String.length s - 10) in
      "\n#elif " ^ s) s in
  let r = Str.regexp @@ " *\\[%%if_\\([A-Za-z0-9]\\|_\\)+\\( \\|\\\n\\)" ^ any_reg ^ "\\]\\\n*" in
  let s = Str.global_substitute r (fun s ->
      let s = String.trim @@ Str.matched_string s in
      let s = return_after_env @@ String.trim @@ String.sub s 6 (String.length s - 7) in
      "#if " ^ s ^ "\n") s in
  let r = Str.regexp @@ "(\\[%if_\\([A-Za-z0-9]\\|_\\)+\\( \\|\\\n\\)" ^ any_reg ^ "\\])\\\n*" in
  let s = Str.global_substitute r (fun s ->
      let s = String.trim @@ Str.matched_string s in
      let s = return_after_env @@ String.trim @@ String.sub s 6 (String.length s - 8) in
      "\n#if " ^ s) s in
  let r = Str.regexp "\\[@@@define [A-Za-z0-9]+\\]" in
  let s = Str.global_substitute r (fun s ->
      let s = String.trim @@ Str.matched_string s in
      let s = String.trim @@ String.sub s 11 (String.length s - 12) in
      "#define " ^ s) s in
  let oc = open_out @@ Format.sprintf "%s.mligo" path in
  let cap = String.uppercase_ascii name in
  let s = Format.sprintf "#if !%s\n#define %s\n\n%s\n\n#endif" cap cap s in
  output_string oc s;
  close_out oc;
  Sys.remove (path ^ ".tmp");
  Sys.remove (path ^ ".tmp2")

let () =
  let filename = ref None in
  let stdout = Filename.temp_file "to_mligo_ocp_indent" "" in
  ocp_indent := (
    match Sys.command @@ Filename.quote_command ~stdout "ocp-indent" ["--version"] with
      | 0 -> true
      | _ -> false);
  Arg.parse ["--keep-prefix", Arg.Set keep_prefix, "keep record prefix"  ] (fun f -> filename := Some f) "";
  match !filename with
  | None -> ()
  | Some f ->
    dir := Filename.dirname f;
    process @@ Filename.basename @@ Filename.remove_extension f
