let make_tr ?entrypoint ?source_file ?(fee= -1L) ?(gas_limit=Z.minus_one)
    ?(storage_limit=Z.minus_one) ?(counter=Z.zero) ?(amount=0L) ~source ~contract f p =
  let open Tzfunc.Proto in
  let h = Mligo_unix.compile_repr ~format:`hex ?entrypoint ?source_file ~expr:(f p) () in
  let value = Bytes (Tzfunc.H.mk h) in
  let entrypoint = match entrypoint with
    | None | Some "main" -> EPdefault
    | Some e -> EzEncoding.destruct entrypoint_enc.Encoding.json e in
  let parameters = Some {entrypoint; value} in
  {
    man_info = {source; kind = Transaction {amount; destination=contract; parameters}};
    man_numbers = {fee; gas_limit; storage_limit; counter};
    man_metadata = None
  }

let make_or ?entrypoint ?source_file ?(fee= -1L) ?(gas_limit=Z.minus_one)
    ?(storage_limit=Z.minus_one) ?(counter=Z.zero) ?(balance=0L) ~source f storage =
  let open Tzfunc.Proto in
  let storage_h = Mligo_unix.compile_repr ~format:`hex ?entrypoint ?source_file ~kind:`storage ~expr:(f storage) () in
  let contract_h = Mligo_unix.compile_repr ~format:`hex ?entrypoint ?source_file ~kind:`contract () in
  let code = Bytes (Tzfunc.H.mk contract_h) in
  let storage = Bytes (Tzfunc.H.mk storage_h) in
  {
    man_info = {source; kind = Origination {balance; script= {code; storage}}};
    man_numbers = {fee; gas_limit; storage_limit; counter};
    man_metadata = None
  }

let forge_tr ?entrypoint ?source_file ?fee ?gas_limit ?storage_limit ?counter
    ?amount ?remove_failed ?forge_method ?base ?get_pk ~source ~contract f p =
  let op = make_tr ?entrypoint ?source_file ?fee ?gas_limit ?storage_limit
      ?counter ?amount ~source ~contract f p in
  Tzfunc.Node.forge_manager_operations ?remove_failed ?forge_method ?base ?get_pk [ op ]

let forge_or ?entrypoint ?source_file ?fee ?gas_limit ?storage_limit ?counter
    ?balance ?remove_failed ?forge_method ?base ?get_pk ~source f storage =
  let op = make_or ?entrypoint ?source_file ?fee ?gas_limit ?storage_limit
      ?counter ?balance ~source f storage in
  Tzfunc.Node.forge_manager_operations ?remove_failed ?forge_method ?base
    ?get_pk [ op ]

let call ?entrypoint ?source_file ?fee ?gas_limit ?storage_limit ?counter
    ?amount ?remove_failed ?forge_method ?base ?get_pk ~source ~contract ~sign f p =
  Lwt.bind (forge_tr ?entrypoint ?source_file ?fee ?gas_limit ?storage_limit
              ?counter ?amount ?remove_failed ?forge_method ?base ?get_pk ~source ~contract f p) @@ function
  | Error e -> Lwt.return_error e
  | Ok (bytes, protocol, branch, ops) ->
    Tzfunc.Node.inject ?base ~sign ~bytes ~branch ~protocol ops

let deploy ?entrypoint ?source_file ?fee ?gas_limit ?storage_limit ?counter
    ?balance ?remove_failed ?forge_method ?base ?get_pk ~source ~sign f storage =
  Lwt.bind (forge_or ?entrypoint ?source_file ?fee ?gas_limit ?storage_limit
              ?counter ?balance ?remove_failed ?forge_method ?base ?get_pk ~source f storage) @@ function
  | Error e -> Lwt.return_error e
  | Ok (bytes, protocol, branch, ops) ->
    Tzfunc.Node.inject ?base ~sign ~bytes ~branch ~protocol ops
