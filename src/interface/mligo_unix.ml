let read ic =
  let rec aux acc =
    try
      let s = input_line ic in
      aux (acc ^ " " ^ String.trim s)
    with _ -> acc in
  String.trim @@ aux ""

let string_of_format = function
  | `json -> "json"
  | `hex -> "hex"
  | `text -> "text"

let compile_repr ?(format=`json) ?(entrypoint="main") ?source_file ?(kind=`parameter) ?expr () =
  let format = string_of_format format in
  let cmd = match source_file, kind, expr with
    | Some file, `parameter, Some expr ->
      Format.sprintf "ligo compile parameter %s %s --michelson-format %s -e %s"
        file expr format entrypoint
    | Some file, `storage, Some expr ->
      Format.sprintf "ligo compile storage %s %s --michelson-format %s -e %s"
        file expr format entrypoint
    | Some file, `contract, _ ->
      Format.sprintf "ligo compile contract %s --michelson-format %s -e %s"
        file format entrypoint
    | _ -> failwith "No source_file or entrypoint or expression given" in
  let ic = Unix.open_process_in cmd in
  let s = read ic in
  close_in ic;
  s
