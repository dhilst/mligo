open Std
open List

type ('key, 'value) map = ('key * 'value) Stdlib.List.t
val empty : ('key, 'value) map
val literal : ('key * 'value) list -> ('key, 'value) map
val find_opt : 'key -> ('key, 'value) map -> 'value option
val update: 'key -> 'value option -> ('key, 'value) map -> ('key, 'value) map
val get_and_update : 'key -> 'value option -> ('key, 'value) map -> 'value option * ('key, 'value) map
val add : 'key -> 'value -> ('key, 'value) map  -> ('key, 'value) map
val remove : 'key -> ('key, 'value) map -> ('key, 'value) map
val iter : (('key * 'value) -> unit) -> ('key, 'value) map -> unit
val map : (('key * 'value) -> ('mapped_key * 'mapped_value)) -> ('key, 'value) map  -> ('mapped_key, 'mapped_value) map
val fold : (('accumulator * ('key * 'value)) -> 'accumulator) -> ('key, 'value) map -> 'accumulator -> 'accumulator
val size : ('key, 'value) map -> nat
val mem : 'key -> ('key, 'value) map -> bool
