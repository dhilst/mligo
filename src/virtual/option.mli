val unopt : 'a option -> 'a
val unopt_with_error : 'a option -> string -> 'a
val map : ('a -> 'b) -> 'a option -> 'b option
