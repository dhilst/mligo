type ('key, 'value) big_map = ('key * 'value) Stdlib.List.t
val empty : ('key, 'value) big_map
val literal : ('key * 'value) list -> ('key, 'value) big_map
val find_opt : 'key -> ('key, 'value) big_map -> 'value option
val mem : 'key -> ('key, 'value) big_map -> bool
val update: 'key -> 'value option -> ('key, 'value) big_map -> ('key, 'value) big_map
val get_and_update : 'key -> 'value option -> ('key, 'value) big_map -> 'value option * ('key, 'value) big_map
val add : 'key -> 'value -> ('key, 'value) big_map  -> ('key, 'value) big_map
val remove: 'key -> ('key, 'value) big_map -> ('key, 'value) big_map
