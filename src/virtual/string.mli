open Std
type nonrec string = string
val length : string -> nat
val sub : nat -> nat -> string -> string
val concat : string -> string -> string
