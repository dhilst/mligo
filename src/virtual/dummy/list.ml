type 'a list = 'a Stdlib.List.t = [] | (::) of 'a * 'a list
let length = Stdlib.List.length
let size = Stdlib.List.length
let head_opt l = try Some (Stdlib.List.hd l) with _ -> None
let tail_opt l = try Some (Stdlib.List.tl l) with _ -> None
let iter f l = Stdlib.List.iter f l
let map f l = Stdlib.List.map f l
let fold f l acc =
  Stdlib.List.fold_left (fun acc x -> f (acc, x)) acc l
let fold_left f acc l = Stdlib.List.fold_left (fun acc x -> f (acc, x)) acc l
let fold_right f l acc = Stdlib.List.fold_right (fun x acc -> f (x, acc)) l acc
