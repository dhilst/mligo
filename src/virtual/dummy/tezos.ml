open Std

type ('param, 'storage, 'res) view = {
  vstorage: 'storage;
  view: ('param * 'storage) -> 'res
}

type context = {
  source: address;
  mutable sender: address;
  mutable self: address;
  level: int;
  time: timestamp;
  voting_power: nat;
  mutable amount: tez;
  chain_id: string;
  mutable contracts: (string, string) contract Stdlib.List.t;
  mutable views: ((address * string) * (string, string, string) view) list;
  constants: (string * (unit -> string)) list;
}

let get_balance = function
  | None -> failwith "no context given"
  | Some ctx ->
    match Stdlib.List.find_opt (fun c -> c.address = ctx.self) ctx.contracts with
    | None -> failwith "self not in context"
    | Some c -> c.balance

let get_now = function
  | None -> failwith "no context given"
  | Some ctx -> ctx.time

let get_amount = function
  | None -> failwith "no context given"
  | Some ctx -> ctx.amount

let get_sender = function
  | None -> failwith "no context given"
  | Some ctx -> ctx.sender

let address c = c.address

let get_self_address = function
  | None -> failwith "no context given"
  | Some ctx -> ctx.self

let self ctx e = match ctx with
  | None -> failwith "no context given"
  | Some ctx ->
    match Stdlib.List.find_opt (fun c ->
        c.address = ctx.self && c.entrypoint_name = e) ctx.contracts with
    | None -> failwith (Format.sprintf "No entrypoint named %S for %S in context" e ctx.self)
    | Some c ->
      { c with storage = Marshal.from_string c.storage 0;
               entrypoint_fun = (fun (p, s) ->
                   let ops, s = c.entrypoint_fun (Marshal.to_string p [], Marshal.to_string s []) in
                   ops, Marshal.from_string s 0) }

let get_source = function
  | None -> failwith "no context given"
  | Some ctx -> ctx.source

let get_chain_id = function
  | None -> failwith "no context given"
  | Some ctx -> ctx.chain_id

let get_total_voting_power = function
  | None -> failwith "no context given"
  | Some ctx -> ctx.voting_power

let voting_power ctx _ = match ctx with
  | None -> failwith "no context given"
  | Some ctx -> ctx.voting_power

let transaction ctx param amount c =
  match ctx with
  | None -> failwith "no context given"
  | Some ctx ->
    let old_sender = ctx.sender in
    let old_self = ctx.self in
    let old_amount = ctx.amount in
    ctx.sender <- old_self;
    ctx.self <- c.address;
    ctx.amount <- amount;
    let operations, storage = c.entrypoint_fun (param, c.storage) in
    ctx.sender <- old_sender;
    ctx.self <- old_self;
    ctx.amount <- old_amount;
    let contracts = Stdlib.List.map (fun a ->
        if a.address = c.address then
          { a with storage = Marshal.to_string storage []; balance = a.balance - amount }
        else a) ctx.contracts in
    ctx.contracts <- contracts;
    let views = Stdlib.List.map (fun ((a, e), v) ->
        if a = c.address then
          (a, e), {v with vstorage = Marshal.to_string storage []}
        else (a, e), v) ctx.views in
    ctx.views <- views;
    { Std.sender = ctx.self; source = ctx.source; amount; destination = c.address;
      entrypoint = c.entrypoint_name; param = Marshal.to_string param [];
      operations }

let set_delegate ctx delegate = match ctx with
  | None -> failwith "no context given"
  | Some ctx ->
    let contracts = Stdlib.List.map (fun a ->
        if a.address = ctx.self then { a with delegate }
        else a) ctx.contracts in
    ctx.contracts <- contracts;
    let param = match delegate with None -> "None" | Some d -> d in
    { Std.sender = ctx.self; source = ctx.source; amount=0; destination = ctx.self;
      entrypoint = "set_delegate"; param; operations = [] }

let get_entrypoint_opt ctx e addr = match ctx with
  | None -> failwith "no context given"
  | Some ctx ->
    Stdlib.List.find_map (fun a ->
        if a.address = addr && a.entrypoint_name = e then
          Some { a with
                 storage = Marshal.from_string a.storage 0;
                 entrypoint_fun = (fun (p, s) ->
                     let ops, s = a.entrypoint_fun (Marshal.to_string p [], Marshal.to_string s []) in
                     ops, Marshal.from_string s 0) }
        else None) ctx.contracts

let get_contract_opt ctx addr = get_entrypoint_opt ctx "%default" addr
let get_contract_with_error ctx addr err = match get_entrypoint_opt ctx "%default" addr with
  | None -> failwith err
  | Some c -> c

let get_level = function
  | None -> failwith "co context given"
  | Some ctx -> ctx.level

let implicit_account ctx address =
  let c = { address; balance = 0; delegate = None; storage = (); entrypoint_name = "%default";
            entrypoint_fun = (fun ((), ()) -> [], ()) } in
  match ctx with
  | None -> c
  | Some ctx ->
    match Stdlib.List.find_opt (fun a -> a.address = address) ctx.contracts with
    | None -> c
    | Some a -> { c with balance = a.balance; delegate = a.delegate }

let open_chest _ctx _k _chest _n = Fail_decrypt
let call_view ctx e arg addr =
  match ctx with
  | None -> None
  | Some ctx ->
    let old_sender = ctx.sender in
    let old_self = ctx.self in
    let old_amount = ctx.amount in
    ctx.sender <- old_self;
    ctx.self <- addr;
    ctx.amount <- 0;
    match Stdlib.List.assoc_opt (addr, e) ctx.views with
    | None -> None
    | Some v ->
      let r = Marshal.from_string (v.view (Marshal.to_string arg [], v.vstorage)) 0 in
      ctx.sender <- old_sender;
      ctx.self <- old_self;
      ctx.amount <- old_amount;
      r

let create_contract ctx main delegate balance storage =
  match ctx with
  | None -> failwith "no context given"
  | Some ctx ->
    let address = Tzfunc.Crypto.(
        Base58.encode ~prefix:Prefix.contract_public_key_hash @@
        Raw.mk @@ Bigstring.to_string @@ Hacl.Rand.gen 20) in
    ctx.contracts <- ctx.contracts @ [ {
        address; balance; delegate; storage = Marshal.to_string storage [];
        entrypoint_name = "default";
        entrypoint_fun = (fun (p, s) ->
      let ops, s = main (Marshal.from_string p 0, Marshal.from_string s 0) in
      ops, Marshal.to_string s [])
      } ];
    let operation = {
      Std.sender = ctx.sender; source = ctx.source; amount = balance;
      destination = address;
      entrypoint = "origination"; param = "";
      operations = [] } in
    operation, address

let constant ctx s =
  match ctx with
  | None -> failwith "no context given"
  | Some ctx -> match Stdlib.List.assoc_opt s ctx.constants with
    | None -> failwith ( s ^ " not given in context constants" )
    | Some f -> Marshal.from_string (f ()) 0

let mk_implicit_account ?(balance=0) ?delegate address = {
  address; balance; delegate; storage = Marshal.to_string () [];
  entrypoint_name = "%default"; entrypoint_fun = (fun (_, s) -> [], s)
}

let mk_contract ?(balance=0) ?delegate ~storage ~entrypoint ~address f = {
  address; balance; delegate; storage = Marshal.to_string storage [];
  entrypoint_name = "%" ^ entrypoint;
  entrypoint_fun = (fun (p, s) ->
      let ops, s = f (Marshal.from_string p 0, Marshal.from_string s 0) in
      ops, Marshal.to_string s [])
}

let mk_view ~storage ~entrypoint ~address f =
  (address, entrypoint),
  {vstorage=Marshal.to_string storage [];
   view = (fun (p, s) ->
       let r = f (Marshal.from_string p 0, Marshal.from_string s 0) in
       Marshal.to_string r [])}
