type nonrec string = string
let length = Stdlib.String.length
let sub i j s = Stdlib.String.sub s i j
let concat a b = a ^ b
