module type S = sig
  type address = string
  type chain_id = string
  type key = string
  type key_hash = string
  type nonrec int = int
  type nat = int
  type signature = string
  type tez = int
  type timestamp = int
  type chest = unit
  type chest_key = string
  type chest_opening_result =
    | Ok_opening of bytes
    | Fail_decrypt
    | Fail_timelock

  type ('param, 'storage) contract = {
    address: address;
    balance: tez;
    delegate: key_hash option;
    storage: 'storage;
    entrypoint_name: string;
    entrypoint_fun: (('param * 'storage) -> (operation list * 'storage));
  }

  and operation = {
    sender: address;
    source: address;
    destination: address;
    entrypoint: string;
    amount: tez;
    param: string;
    operations: operation list }

  val is_nat : int -> nat option
  val abs : int -> nat
  val int : nat -> int
  val unit : unit
  val ediv : int -> int -> (int * nat) option

  val failwith : string -> 'a
  val failwith_int : int -> 'a
end

type address = string
type chain_id = string
type key = string
type key_hash = string
type nonrec int = int
type nat = int
type signature = string
type tez = int
type timestamp = int
type chest = unit
type chest_key = string
type chest_opening_result =
  | Ok_opening of bytes
  | Fail_decrypt
  | Fail_timelock

type ('param, 'storage) contract = {
  address: address;
  balance: tez;
  delegate: key_hash option;
  storage: 'storage;
  entrypoint_name: string;
  entrypoint_fun: (('param * 'storage) -> (operation list * 'storage));
}

and operation = {
  sender: address;
  source: address;
  destination: address;
  entrypoint: string;
  amount: tez;
  param: string;
  operations: operation list}

let is_nat n = if n > 0 then Some n else None
let abs n = abs n
let int n = n
let unit = ()
let ediv n d =
  if d = 0 then None else Some (n / d, n mod d)

let failwith s = failwith s
let failwith_int i = failwith (string_of_int i)
