let _and a b = a land b
let _or a b = a lor b
let xor a b = a lxor b
let shift_left a b = a lsl b
let shift_right a b = a lsr b
