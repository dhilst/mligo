open Std

let unopt x = Stdlib.Option.get x
let unopt_with_error x s = match x with
  | None -> failwith s
  | Some x -> x
let map f x = Stdlib.Option.map f x
