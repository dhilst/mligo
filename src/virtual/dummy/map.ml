type ('key, 'value) map = ('key * 'value) Stdlib.List.t
let empty : ('k, 'v) map = []
let literal (l : ('k * 'v) List.list) : ('k, 'v) map = l
let find_opt k l = Stdlib.List.assoc_opt k l
let mem k l = Stdlib.Option.is_some (Stdlib.List.assoc_opt k l)
let update k v l =
  let l = Stdlib.List.remove_assoc k l in
  match v with None -> l | Some v -> (k, v) :: l
let get_and_update k v l =
  let v2 = Stdlib.List.assoc_opt k l in
  let l = Stdlib.List.remove_assoc k l in
  match v with None -> v2, l | Some v -> v2, (k, v) :: l
let add k v l =
  if mem k l then
    (Std.failwith "binding already exists" : _ map)
  else (k, v) :: l
let remove k l = Stdlib.List.remove_assoc k l

let iter f l = Stdlib.List.iter f l
let map f l = Stdlib.List.map f l
let fold f l acc =
  Stdlib.List.fold_left (fun acc x -> f (acc, x)) acc l
let size l = Stdlib.List.length l
