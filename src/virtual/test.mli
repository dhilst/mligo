open Std

type michelson_program
type ligo_program
type test_exec_error =
  | Rejected of (michelson_program * address)
  | Other
type test_exec_result =
  | Success
  | Fail of test_exec_error
type ('p, 's) typed_address
type mutation

val to_contract : ('p, 's) typed_address -> ('p, 's) contract
val to_entrypoint : string -> ('p, 's) typed_address -> ('e, _) contract
val originate_from_file :
  string -> string -> string list -> michelson_program -> tez ->
  (address * michelson_program * int)
val originate :
  ('parameter * 'storage -> operation List.list * 'storage) -> 'storage -> tez ->
  (('parameter, 'storage) typed_address * michelson_program * int)
val set_now : string -> unit
val set_source : address -> unit
val set_baker : address -> unit
val transfer : address -> michelson_program -> tez -> test_exec_result
val transfer_exn : address -> michelson_program -> tez -> unit
val transfer_to_contract : ('p, _) contract -> 'p -> tez -> test_exec_result
val transfer_to_contract_exn : ('p, _) contract -> 'p -> tez -> unit
val get_storage_of_address : address -> michelson_program
val get_storage : ('p, 's) typed_address -> 's
val get_balance : address -> tez
val michelson_equal : michelson_program -> michelson_program -> bool
val log : 'a -> unit
val reset_state : nat -> nat List.list -> unit
val nth_bootstrap_account : int -> address
val compile_expression : string option -> ligo_program -> michelson_program
val compile_expression_subst : string option -> ligo_program -> (string * michelson_program) List.list -> michelson_program
val compile_value : 'a -> michelson_program
val eval : 'a -> michelson_program
val run : ('a -> 'b) -> 'a -> michelson_program
val mutate_count : ligo_program -> nat
val mutate_expression : nat -> ligo_program -> nat
val mutate_value : nat -> 'a -> ('a * mutation) option
val mutation_test : 'a -> ('a -> 'b) -> ('b * mutation) option
val mutation_test_all : 'a -> ('a -> 'b) -> ('b * mutation) List.list
val create_chest : bytes -> nat -> chest * chest_key
val create_chest_key : chest -> nat -> chest_key
