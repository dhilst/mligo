open Std
open List

type 'value set = 'value Stdlib.List.t
val empty : 'value set
val literal : 'value list -> 'value set
val mem : 'value -> 'value set -> bool
val cardinal : 'value set -> nat
val add : 'value -> 'value set -> 'value set
val remove : 'value -> 'value set -> 'value set
val update : 'a -> bool -> 'a set -> 'a set
val iter : ('a -> unit) -> 'a set -> unit
val fold : (('accumulator * 'item) -> 'accumulator) -> 'item set -> 'accumulator -> 'accumulator
val fold_desc : (('item * 'accumulator) -> 'accumulator) -> 'item set -> 'accumulator -> 'accumulator
