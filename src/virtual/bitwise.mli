open Std
val _and : nat -> nat -> nat
val _or :  nat -> nat -> nat
val xor :  nat -> nat -> nat
val shift_left :  nat -> nat -> nat
val shift_right :  nat -> nat -> nat
