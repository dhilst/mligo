module Std = Std
module Big_map = Big_map
module Bitwise = Bitwise
module Bytes = Bytes
module Crypto = Crypto
module List = List
module Map = Map
module Option = Option
module Set = Set
module String = String
module Tezos = Tezos
module Test = Test

include Std.S

type nonrec string = String.string
type nonrec bytes = Bytes.bytes
type nonrec 'a list = 'a List.list = [] | (::) of 'a * 'a list
type ('key, 'value) big_map = ('key, 'value) Big_map.big_map
type ('key, 'value) map = ('key, 'value) Map.map
type 'value set = 'value Set.set

type ('param, 'storage) contract = ('param, 'storage) Std.contract = {
  address: address;
  balance: tez;
  delegate: key_hash option;
  storage: 'storage;
  entrypoint_name: string;
  entrypoint_fun: (('param * 'storage) -> (operation list * 'storage));
}

and operation = Std.operation = {
  sender: address;
  source: address;
  destination: address;
  entrypoint: string;
  amount: tez;
  param: string;
  operations: operation list}
